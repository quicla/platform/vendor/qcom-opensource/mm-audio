/***
Module_codec_control.c -- Pulseaudio module codec control is a module
belongs to pulseaudio, which could be loaded by pulseaudio.It used to
enable, disable, postprocess configure for codecs.

Copyright (c) 2016, The Linux Foundation. All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above
      copyright notice, this list of conditions and the following
      disclaimer in the documentation and/or other materials provided
      with the distribution.
    * Neither the name of The Linux Foundation nor the names of its
      contributors may be used to endorse or promote products derived
      from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
***/

#include "module_codec_control.h"
#define LIB_PLUGIN_DRIVER "libaudiohalplugin.so"

PA_MODULE_AUTHOR("AGL Audio");
PA_MODULE_DESCRIPTION("pulseaudio codec control module through libaudiohalplugin.so");
PA_MODULE_VERSION(PACKAGE_VERSION);
PA_MODULE_LOAD_ONCE(false);

static audio_hal_plugin_usecase_type_t audio_codec_get_usecase(pa_core *c, void *pad,
                                                             struct cc_userdata *u,
                                                             audio_hal_plugin_direction_type_t dir,
                                                             const char *role) {
    audio_hal_plugin_usecase_type_t usecase;

    if (dir == AUDIO_HAL_PLUGIN_DIRECTION_PLAYBACK){
        if (role == NULL) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        } else if (!strcmp(role, "phone")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        } else if (!strcmp(role, "video")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        } else if (!strcmp(role, "music")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        } else if (!strcmp(role, "event")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        } else if (!strcmp(role, "navi")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DRIVER_SIDE_PLAYBACK;
        } else if ((!strcmp(role, "hfp_downlink")) || (!strcmp(role, "hfp_uplink"))
                  || (!strcmp(role, "hfp_wb_downlink")) || (!strcmp(role, "hfp_wb_uplink"))) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_HFP_VOICE_CALL;
        } else if (!strcmp(role, "sdars_tuner_local_amp")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        } else {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_PLAYBACK;
        }
   } else {
        if (role == NULL) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
        } else if (!strcmp(role, "phone")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
        } else if (!strcmp(role, "video")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
        } else if (!strcmp(role, "music")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
        } else if (!strcmp(role, "event")) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
        } else if ((!strcmp(role, "hfp_downlink")) || (!strcmp(role, "hfp_uplink"))
                  || (!strcmp(role, "hfp_wb_downlink")) || (!strcmp(role, "hfp_wb_uplink"))) {
            usecase = AUDIO_HAL_PLUGIN_USECASE_HFP_VOICE_CALL;
        }else {
            usecase = AUDIO_HAL_PLUGIN_USECASE_DEFAULT_CAPTURE;
        }
    }
    pa_log_debug("%s role %s usecase %d\n", __FUNCTION__, role, usecase);
    return usecase;
}

static int audio_codec_enable(pa_core *c, void *pad, struct cc_userdata *u, audio_hal_plugin_direction_type_t dir,
	                                                audio_hal_plugin_usecase_type_t usecase) {
    audio_hal_plugin_codec_enable_t codec_enable;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_ENABLE;
    int ret = 0;

    pa_log_debug("%s IN", __FUNCTION__);

    codec_enable.usecase = usecase;
    codec_enable.direction = dir;
    codec_enable.sample_rate = 48000;
    codec_enable.bit_width = 16;
    codec_enable.num_chs = 2;
    ret = u->audio_hal_plugin_send_msg(msg, (void*)&codec_enable, sizeof(codec_enable));
    if (ret) {
        pa_log_error("%s return Failure! ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s OUT", __FUNCTION__);
    return ret;
}

static int audio_codec_disable(pa_core *c, void *pad, struct cc_userdata *u, audio_hal_plugin_direction_type_t dir,
	                                                audio_hal_plugin_usecase_type_t usecase) {
    audio_hal_plugin_codec_disable_t codec_disable;
    audio_hal_plugin_msg_type_t msg = AUDIO_HAL_PLUGIN_MSG_CODEC_DISABLE;
    int ret = 0;

    pa_log_debug("%s IN", __FUNCTION__);

    codec_disable.usecase = usecase;
    codec_disable.direction = dir;
    ret = u->audio_hal_plugin_send_msg(msg, (void*)&codec_disable, sizeof(codec_disable));
    if (ret) {
        pa_log_error("%s return Failure! ret = %d", __FUNCTION__, __LINE__, ret);
        return ret;
    }

    pa_log_debug("%s OUT", __FUNCTION__);
    return ret;
}

static pa_hook_result_t sink_input_put_hook_callback(pa_core *c, pa_sink_input *sink_input, struct cc_userdata *u) {
    const char *role;
    uint8_t role_index =0;
    pa_sink *sink = sink_input->sink;
    int ret = PA_HOOK_OK;
    audio_hal_plugin_usecase_type_t usecase;

    pa_log_debug("%s IN", __FUNCTION__);

    //get media role
    role = pa_proplist_gets(sink_input->proplist, PA_PROP_MEDIA_ROLE);
    if ((role != NULL) && !strcmp(role, "abstract"))
        return ret;

    usecase = audio_codec_get_usecase(c, (void *)sink_input, u, AUDIO_HAL_PLUGIN_DIRECTION_PLAYBACK, role);
    pa_log_debug("%s usecase %d media role %s", __FUNCTION__, usecase, role);

    /* dir 0 for playback*/
    ret = audio_codec_enable(c, (void*)sink_input, u, AUDIO_HAL_PLUGIN_DIRECTION_PLAYBACK, usecase);
    if(!ret){
        ret = PA_HOOK_OK;
    }else{
        ret = PA_HOOK_CANCEL;
    }

    pa_log_debug("%s OUT", __FUNCTION__);
    return ret;
}

static pa_hook_result_t sink_input_unlink_hook_callback(pa_core *c, pa_sink_input *sink_input, struct cc_userdata *u) {
    const char *role;
    pa_sink *sink = sink_input->sink;
    int ret = PA_HOOK_OK;
    audio_hal_plugin_usecase_type_t usecase;

    pa_log_debug("%s IN", __FUNCTION__);

    //get media role
    role = pa_proplist_gets(sink_input->proplist, PA_PROP_MEDIA_ROLE);
    if ((role != NULL) && !strcmp(role, "abstract"))
        return ret;

    usecase = audio_codec_get_usecase(c, (void *)sink_input, u, AUDIO_HAL_PLUGIN_DIRECTION_PLAYBACK, role);
    pa_log_debug("%s usecase %d media role %s", __FUNCTION__, usecase, role);

    ret = audio_codec_disable(c, sink_input, u, AUDIO_HAL_PLUGIN_DIRECTION_PLAYBACK, usecase);
    if(!ret){
        ret = PA_HOOK_OK;
    }else{
        ret = PA_HOOK_CANCEL;
    }

    pa_log_debug("%s OUT", __FUNCTION__);
    return ret;
}

static pa_hook_result_t source_output_put_hook_callback(pa_core *c, pa_source_output *source_output, struct cc_userdata *u) {
    const char *role;
    pa_source *source = source_output->source;
    int ret = PA_HOOK_OK;
    audio_hal_plugin_usecase_type_t usecase;

    pa_log_debug("%s IN", __FUNCTION__);

    //get media role
    role = pa_proplist_gets(source_output->proplist, PA_PROP_MEDIA_ROLE);
    if ((role != NULL) && !strcmp(role, "abstract"))
        return ret;

    /* CODEC path does not need to be enabled/disabled for the capture path of
     * SDARS tuner use case. The tuner data feeds directly into the DSP from
     * the tuner HW.
     */
    if ((role != NULL) && !strcmp(role, "sdars_tuner_local_amp"))
        return ret;

    usecase = audio_codec_get_usecase(c, (void *)source_output, u, AUDIO_HAL_PLUGIN_DIRECTION_CAPTURE, role);
    pa_log_debug("%s usecase %d media role %s", __FUNCTION__, usecase, role);

    ret = audio_codec_enable(c, (void*)source_output, u, AUDIO_HAL_PLUGIN_DIRECTION_CAPTURE, usecase);
    if(!ret){
        ret = PA_HOOK_OK;
    }else{
        ret = PA_HOOK_CANCEL;
    }

    pa_log_debug("%s OUT", __FUNCTION__);
    return ret;
}

static pa_hook_result_t source_output_unlink_hook_callback(pa_core *c, pa_source_output *source_output, struct cc_userdata *u) {
    const char *role;
    pa_source *source = source_output->source;
    int ret = PA_HOOK_OK;
    audio_hal_plugin_usecase_type_t usecase;

    pa_log_debug("%s IN", __FUNCTION__);

    //get media role
    role = pa_proplist_gets(source_output->proplist, PA_PROP_MEDIA_ROLE);
    if ((role != NULL) && !strcmp(role, "abstract"))
        return ret;

    /* CODEC path does not need to be enabled/disabled for the capture path of
     * SDARS tuner use case. The tuner data feeds directly into the DSP from
     * the tuner HW.
     */
    if ((role != NULL) && !strcmp(role, "sdars_tuner_local_amp"))
        return ret;

    usecase = audio_codec_get_usecase(c, (void *)source_output, u, AUDIO_HAL_PLUGIN_DIRECTION_CAPTURE, role);
    pa_log_debug("%s usecase %d media role %s", __FUNCTION__, usecase, role);

    ret = audio_codec_disable(c, source_output, u, AUDIO_HAL_PLUGIN_DIRECTION_CAPTURE, usecase);
    if(!ret){
        ret = PA_HOOK_OK;
    }else{
        ret = PA_HOOK_CANCEL;
    }

    pa_log_debug("%s OUT", __FUNCTION__);
    return ret;
}

int pa__init(pa_module*m) {
    struct cc_userdata *u;
    const char *description;
    char *fn = NULL;
    bool namereg_fail = false;
    void * ptemp = NULL;
    int32_t ret = 0;

    pa_log_info("Start Loading Codec Control module");

    pa_assert(m);

    m->userdata = u = pa_xnew0(struct cc_userdata, 1);
    u->core = m->core;
    u->module = m;

    /* hook start of sink input/source output to enable modifiers */
    /* A little bit later than module-role-cork */
    u->sink_input_put_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SINK_INPUT_PUT], PA_HOOK_LATE+3, (pa_hook_cb_t) sink_input_put_hook_callback, u);
    u->source_output_put_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SOURCE_OUTPUT_PUT], PA_HOOK_LATE+3, (pa_hook_cb_t) source_output_put_hook_callback, u);

    /* A little bit later than module-stream-restore, a little bit earlier than module-intended-roles, module-rescue-streams, ... */
    u->sink_input_unlink_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SINK_INPUT_UNLINK], PA_HOOK_LATE+3, (pa_hook_cb_t) sink_input_unlink_hook_callback, u);
    u->source_output_unlink_hook_slot = pa_hook_connect(&m->core->hooks[PA_CORE_HOOK_SOURCE_OUTPUT_UNLINK], PA_HOOK_LATE+3, (pa_hook_cb_t) source_output_unlink_hook_callback, u);

    u->plugin_handle = dlopen(LIB_PLUGIN_DRIVER, RTLD_NOW);
    if (u->plugin_handle == NULL) {
        pa_log_error("DLOPEN failed for %s %s", LIB_PLUGIN_DRIVER, dlerror());
        goto fail;
    } else {
        pa_log_debug("DLOPEN successful for %s", LIB_PLUGIN_DRIVER);
        u->audio_hal_plugin_init = (audio_hal_plugin_init_t)dlsym(
                        u->plugin_handle, "audio_hal_plugin_init");
        if (!u->audio_hal_plugin_init) {
            pa_log_error("Could not find the symbol audio_hal_plugin_init from %s",LIB_PLUGIN_DRIVER);
            goto fail;
        }

        u->audio_hal_plugin_deinit = (audio_hal_plugin_deinit_t)dlsym(
                        u->plugin_handle, "audio_hal_plugin_deinit");
        if (!u->audio_hal_plugin_deinit) {
            pa_log_error("Could not find the symbol audio_hal_plugin_deinit from %s", LIB_PLUGIN_DRIVER);
            goto fail;
        }

        u->audio_hal_plugin_send_msg = (audio_hal_plugin_send_msg_t)
                        dlsym(u->plugin_handle, "audio_hal_plugin_send_msg");
        if (!u->audio_hal_plugin_send_msg) {
            pa_log_error("Could not find the symbol audio_hal_plugin_send_msg from %s", LIB_PLUGIN_DRIVER);
            goto fail;
        }

        ret = u->audio_hal_plugin_init();
        if (ret) {
            pa_log_error("audio_hal_plugin_init failed with ret = %d", ret);
            goto fail;
        }
    }

    pa_log_info("Load Codec Control Module Success!!!");
    return 0;
fail:
    if(u->plugin_handle != NULL)
        dlclose(u->plugin_handle);

    pa_log_info("Load Codec Control Module Failure!!!");
    return -1;
}


void pa__done(pa_module*m) {
    struct cc_userdata* u;
    int32_t ret = 0;

    pa_log_info("Start Unloading Codec Control Module");

    pa_assert(m);

    if (!(u = m->userdata))
        return;

    if (u->sink_input_put_hook_slot)
        pa_hook_slot_free(u->sink_input_put_hook_slot);

    if (u->sink_input_unlink_hook_slot)
        pa_hook_slot_free(u->sink_input_unlink_hook_slot);

    if (u->source_output_put_hook_slot)
        pa_hook_slot_free(u->source_output_put_hook_slot);

    if (u->source_output_unlink_hook_slot)
        pa_hook_slot_free(u->source_output_unlink_hook_slot);

    if (u->audio_hal_plugin_deinit) {
    ret = u->audio_hal_plugin_deinit();
        if (ret) {
            pa_log_error("audio_hal_plugin_deinit failed with ret = %d", ret);
        }
    }

    if(u->plugin_handle != NULL)
        dlclose(u->plugin_handle);

    pa_xfree(u);

    pa_log_info("Unload Codec Control Module Done");
}
